# signal library for janet

Provide bindings to `sigwait`, `raise`, `kill`, `sigprocmask` and etc.

Check [example](test/interactive.janet) for usage.

Tested on 
- [x] Linux

