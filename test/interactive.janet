(import ../src/init :prefix "sig/")

(defmacro pp "Pretty print symbol's name and value" [& vars]
  (defn print-single [v]
    (with-syms [$v] ~(let [,$v ,v]
    (prinf "%s=%p " ,(string/format "%p" v) ,$v))))
  # ~'(do ,;(map print-single vars)))
  ~(do ;,(map print-single vars) (print)))

(defn test_1 []
  (def sigmask (sig/mask/from-names [:INT]))
  (sig/procmask :setmask sigmask nil)
  (print "Press ^C or send SIGINT to me")
  (printf "Received signal: %v" (sig/wait sigmask)))

(test_1)
(print "Hello")
(sig/raise :QUIT)
(print "I'm not supposed to be alive")
