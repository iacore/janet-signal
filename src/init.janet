(use ./raw)

(def- signum-map {
  :INT  SIGINT  
  :ILL  SIGILL  
  :ABRT SIGABRT 
  :FPE  SIGFPE  
  :SEGV SIGSEGV 
  :TERM SIGTERM 
  :HUP  SIGHUP  
  :QUIT SIGQUIT 
  :TRAP SIGTRAP 
  :KILL SIGKILL 
  :PIPE SIGPIPE 
  :ALRM SIGALRM 
})

(defn list-signals "List all signal names"
  [] (keys signum-map))

(def- signum-map-rev (invert signum-map))

(defn- get-signum! [signame]
  (or (signum-map (keyword (string/ascii-upper signame)))
    (errorf "Invalid signal: %p. Choices: %p." signame (keys signum-map))))

(def- procmask-action-map {
  :block   SIG_BLOCK
  :unblock SIG_UNBLOCK
  :setmask SIG_SETMASK
})

(defn- get-procmask-action! [actionname]
  (or (procmask-action-map (keyword (string/ascii-lower actionname)))
    (errorf "Invalid action: %p. Choices: %p." actionname (keys procmask-action-map))))

(defn- assert-posix "Throw if value < -1. Useful for POSIX functions."
[ret]
  (if (< ret 0)
    (error "TODO: throw errno"))
  ret)

(defn mask/from-names [arr]
  (def sigmask (ffi/write sigset_t (array/new-filled 128 0)))
  (assert-posix (sigemptyset sigmask))
  (each signame arr
    (def signum (get-signum! signame))
    (assert-posix (sigaddset sigmask signum)))
  sigmask)

(defn mask/to-names [sigmask]
  (def res @[])
  (each [signame signum] (pairs signum-map)
    (if (= 1 (assert-posix (sigismember sigmask signum)))
      (array/push res signame)))
  res)

(defn procmask [action inmask outmask]
  (def actionnum (get-procmask-action! action))
  (assert-posix (sigprocmask actionnum inmask outmask)))

(defn wait "Wait for a signal."
[sigmask]
  (def sig (ffi/write ptr_int [0]))
  (assert-posix (sigwait sigmask sig))
  (def signum ((ffi/read ptr_int sig) 0))
  (or (signum-map-rev signum) (errorf "Unknown signal: %v" signum)))

(def- _raise raise)

(defn raise "Send a signal to the running process." [signame]
  (assert-posix (_raise (get-signum! signame))))

(def- _kill kill)

(defn kill "See manual for kill (3)." [pid signame]
  (assert-posix (_kill pid (get-signum! signame))))
