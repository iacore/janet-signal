# /* ISO C99 signals.  */
(def SIGINT  "Interactive attention signal."   2)
(def SIGILL  "Illegal instruction."            4)
(def SIGABRT "Abnormal termination."           6)
(def SIGFPE  "Erroneous arithmetic operation." 8)
(def SIGSEGV "Invalid access to storage."      11)
(def SIGTERM "Termination request."            15)

# /* Historical signals specified by POSIX. */
(def SIGHUP  "Hangup."                         1)
(def SIGQUIT "Quit."                           3)
(def SIGTRAP "Trace/breakpoint trap."          5)
(def SIGKILL "Killed."                         9)
(def SIGPIPE "Broken pipe."                    13)
(def SIGALRM "Alarm clock."                    14)

(def SIG_BLOCK   "for blocking signals"        0)
(def SIG_UNBLOCK "for unblocking signals"      1)
(def SIG_SETMASK "for setting the signal mask" 2)

(def sigset_t @[:u8 128])
(def ffi_sigset_t :ptr)

(def ptr_int [:int])
(def ffi_ptr_int :ptr)

(ffi/context)

# int sigemptyset(sigset_t *set);
(ffi/defbind
  sigemptyset :int
  [set ffi_sigset_t])

# int sigfillset(sigset_t *set);
(ffi/defbind
  sigfillset :int
  [set ffi_sigset_t])

# int sigaddset(sigset_t *set, int signum);
(ffi/defbind
  sigaddset :int
  [set ffi_sigset_t signum :int])

# int sigdelset(sigset_t *set, int signum);
(ffi/defbind
  sigdelset :int
  [set ffi_sigset_t signum :int])

# int sigismember(const sigset_t *set, int signum);
(ffi/defbind
  sigismember :int
  [set ffi_sigset_t signum :int])

# int sigprocmask(int how, const sigset_t *restrict input, sigset_t *restrict output);
(ffi/defbind
  sigprocmask :int  "Set current process's signal mask (what signal to block). Blocks the thread."
  [how :int set ffi_sigset_t oset ffi_sigset_t])

# int sigwait(const sigset_t *restrict set, int *restrict sig);
(ffi/defbind
  sigwait :int  "Wait for a signal in `set`. Blocks the thread."
  [set ffi_sigset_t sig ffi_ptr_int])

# int sigsuspend(const sigset_t *sigmask);
(ffi/defbind
  sigsuspend :int  "Suspend until one signal handler is called and finished. Blocks the thread."
  [sigmask ffi_sigset_t])


(def ffi_pid_t :int)

# int kill(pid_t pid, int sig);
(ffi/defbind
  kill :int  "Kill a process."
  [pid ffi_pid_t sig :int])

# int raise(int sig);
(ffi/defbind
  raise :int  "Kill the calling process."
  [sig :int])

# int sigtimedwait(const sigset_t *restrict set,
#     siginfo_t *restrict info,
#     const struct timespec *restrict timeout);
# int sigwaitinfo(const sigset_t *restrict set,
#     siginfo_t *restrict info);
