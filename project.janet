(declare-project
  :name "signal"
  :description ```POSIX signal handling library```
  :version "0.0.0")

(declare-source
  :prefix "signal"
  :source [
    "src/init.janet"
    "src/raw.janet"
  ]
)
